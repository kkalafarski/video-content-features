"""
master_script.py

Usage
master_script.py /full/path/to/file.mp4

Output:
File details:
	full path	
	file size	
	md5sum hash
Start and end date for video processing (for each file)
Frames counter, 
Confirmation that data were stored as csv


Output files:
results.csv - row indicated single file, column indicated metrics (2D matrix)
results_frame.csv - row indicated single file, column indicated frames, each cell have list of  metrics values (3D matrix)


"""

from  skimage import feature
from matplotlib import pyplot as plt
from scipy import ndimage
from scipy import signal
from scipy.signal import sepfir2d
from skimage import data
from skimage.measure import compare_ssim as ssim
from skimage.measure import shannon_entropy as entropy
from datetime import datetime
import csv
import cv2
import hashlib
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import numpy.matlib
import re
import scipy
import scipy.stats
import time,sys,os
import webbrowser
import sys
import os
import time


def save_data_as_csv(csv_filename, str, results_list):
	""" 
	save_data_as_csv(STRING, LIST)
	Function:
		add file name (extracted from STRING) to LIST
	    open results_[filename].csv file and append it with given LIST
		append result.csv with LIST
		
	"""
	#file_name = extract_file_name(str)
	file_name = str
	results_list.insert(0,file_name)
	#timestamp = time.strftime("%Y%m%d-%H%M%S")
	with open("results_%s.csv" % csv_filename, "ab") as f:
		wr = csv.writer(f, quoting=csv.QUOTE_ALL)
		wr.writerow(results_list)
	print "CSV updated for data forma:",csv_filename

def save_frame_data_as_csv(str, results_list):
	""" 
	save_data_as_csv(STRING, LIST)
	Function:
		add file name (extracted from STRING) to LIST
	    open results_frame.csv file and append it with given LIST
		append results_frame.csv with LIST
		
		
	To be done:
	Calculate geometric mean 
		
	"""
        file_name = extract_file_name(str)
        results_list.insert(0,file_name)
	#timestamp = time.strftime("%Y%m%d-%H%M%S")
        with open("results_frame.csv", "a") as f:
                wr = csv.writer(f, quoting=csv.QUOTE_ALL)
                wr.writerow(results_list)
        print "CSV updated"



def extract_file_name(str):
	"""
	extract_file_name(STRING)
	Function:
		return file name in full path to file
		
	"""
	m = re.search('\w+\d', str)
	if m:
		found = m.group(0)
	else:
		found = 'empty'
	return found
	

def convert_to_mp4(str):
	"""
	
	TDB
	
	"""
	reg = re.compile('/\w+/\w+/\w+/\w+/\w+/\w+[1-9]\d*')
	#with open(str) as f:
	# with open ('/home/kkalafarski/vmaf/batch_vga') as f:
    #	for line in f:
    #       research = re.search(line)
    #       if reserch:
    #           tmp = ("%s") % research.group()
    #           print tmp
    #           print tmp
                # tmp_str = "ffmpeg -f rawvideo -vcodec rawvideo -s 640x480 -r 25 -pix_fmt yuv420p -i " + str + ".yuv -c:v libx264 -preset ultrafast -qp 0 " + str + ".mp4"
                # os.system( tmp_str )
	return str
	
	
##################### MAIN


def metric(inFile):
		frameNumber = 0
		success = True
		tmp = [[] for i in range(35)]
		average_results_list = []
		vidcap = cv2.VideoCapture(inFile)
		print str(datetime.now())
		while success:
                	success, image = vidcap.read()
                	if (image is not None):
					#print str(datetime.now())
					
					print "Frame no:", frameNumber + 1
					## Change frame from RGB to YUV"
					# Metrics:  Chrominance and Contrast Information
					img_yuv = cv2.cvtColor( image, cv2.COLOR_BGR2YUV)
					
					## Change frame from RGB to gray scale"
					# Metrics:  Spatial information
					# 		 	GLCM - Gray-Level Co-occurrence Matrix
					#			Laplacian based features
					
					img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
					
					
					## Split yuv to  y, u, v" 
					y, u, v = cv2.split(img_yuv)
		            # apply gradient filter, type: Laplacian with kernel size=3
                    # calculate SOBEL function for x and y axis, combine both with weight 0.5 to obtain result
                    ## Metrics: Spatial information"
					laplacian = cv2.Laplacian(img_gray,cv2.CV_64F)
 					sobelx = cv2.Sobel(img_gray, cv2.CV_64F,1,0,ksize=3)
					sobely = cv2.Sobel(img_gray, cv2.CV_64F,0,1,ksize=3)
					sobelxy = sobelx * 0.5 + sobely * 0.5

                    #calulate vaules of Laplacian piramid (5 levels)
                    ## Calculating Laplacina piramid"
					lap5 = cv2.convertScaleAbs(cv2.Laplacian(img_gray,cv2.CV_16S))
					lap4 = cv2.convertScaleAbs(cv2.Laplacian(lap5,cv2.CV_16S))
					lap3 = cv2.convertScaleAbs(cv2.Laplacian(lap4,cv2.CV_16S))
					lap2 = cv2.convertScaleAbs(cv2.Laplacian(lap3,cv2.CV_16S))
					lap1 = cv2.convertScaleAbs(cv2.Laplacian(lap2,cv2.CV_16S))
					lap0 = cv2.convertScaleAbs(cv2.Laplacian(lap1,cv2.CV_16S))
                                       

                    #change lap0 and lap3 to histograms to calculate ssim and kurtosis
					Hlap3 = cv2.calcHist( [lap3], [0], None, [256], [0, 256])
					Hlap0 = cv2.calcHist( [lap0], [0], None, [256], [0, 256])


					#generate iterable list of all metrices
					metrics = [
					# lists that store values for y [index 0], u [index 1], v [index 2]
					## Metrics: Chrominance and Contrast Information"
					np.std( y ),
					np.mean( y ),
					np.amax( y ),
				
					#print "4 scipy.stats.histogram", scipy.stats.skew(scipy.stats.histogram( y ))
					scipy.stats.skew(cv2.calcHist( [y], [0], None, [256], [0, 256] )),
					scipy.stats.kurtosis(cv2.calcHist( [y], [0], None, [256], [0, 256] )),
					
					np.std( u ),
					np.mean( u ),
					np.amax( u ),
					scipy.stats.skew(cv2.calcHist( [u], [0], None, [256], [0, 256] )),
					scipy.stats.kurtosis(cv2.calcHist( [u], [0], None, [256], [0, 256] )),
					np.std( v ),
					
					#add 1.5 * to elements to follow chrominescence information
					np.mean( v ),
					np.amax( v ),
					scipy.stats.skew(cv2.calcHist( [v], [0], None, [256], [0, 256] )),
					scipy.stats.kurtosis(cv2.calcHist( [v], [0], None, [256], [0, 256] )),
					
					
					# lists that store values for std [index 0] and mean [index 1]
					# Metrics: Spatial information
					np.std(sobelxy),
					np.mean(sobelxy),
					np.std(sobelx),
					np.std(sobely),
					
				
				        	
					#calulate vaules of Laplacian piramid (5 levels)
					## Calculating Laplacina piramid"
					entropy(img_gray),
					#E = 0
					entropy(lap5),
					entropy(lap3),
					
					#calculate ssim and kurtosis base on histogram of lap3 and lap0
					scipy.stats.kurtosis(Hlap3),
					scipy.stats.kurtosis(Hlap0),
					ssim(lap0,lap3)]
					
					# Map list to more readable format
					results_list = list(map(float, metrics))
					
					# Increment frame counter
					frameNumber = frameNumber + 1
					
					# Save data (results_list) by appending empty list (average_results_list)
					average_results_list.append(results_list)
			else:
					print "Frame number: ", frameNumber
					print str(datetime.now())
					print "End of file: empty frame"
					# Generate average value for each metric 
					average = [float(sum(col))/len(col) for col in zip(*average_results_list)]
					# Save results for separate frames
					#save_frame_data_as_csv(inFile, map(list, zip(average_results_list)))
					# Save average results 
					save_data_as_csv('average', inFile, average)
					save_data_as_csv('frame', inFile, map(list, zip(*average_results_list)))
					std = [numpy.std(col) for col in zip(*average_results_list)]
					save_data_as_csv('std', inFile, std)


					
####################################################################### ITERATOR #######################################################################
directory = os.path.dirname(__file__)
for filename in os.listdir(directory):
	if filename.endswith(".avi") or filename.endswith(".ts")  :
		#print(os.path.join(directory, filename))
		print filename
		metric(filename)
		continue
	else:
		continue
		print "End"

#######################################################################  MAIN ##########################################################################				
#input file
#inFile = sys.argv[1]
inFile = "1.ts"
vidcap = cv2.VideoCapture(inFile)
#if inFile = *yuv
#	convert_to_mp4(inFile)
#elsif inFile = *mp4
success, image = vidcap.read()
#else print "not supported format"

# Print details of file
print "File readable: ",  success
print "File path/name: ", inFile
print "File size: ", os.path.getsize(inFile) >> 20, "Mb"
print "md5sum hash: ", hashlib.md5(inFile).hexdigest()

# Run metric for single file
#metric(inFile)
####################################################################### END MAIN #######################################################################
